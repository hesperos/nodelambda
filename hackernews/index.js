const axios = require('axios')
const path = require('path')

const baseUrl = new URL('https://hacker-news.firebaseio.com/v0');

function makeUrl(base, endpoint) {
    return new URL(path.join(base.pathname, endpoint), base.href)
}

function createClient(baseUrl) {
    return {
        baseUrl: baseUrl,
        topStories: function() {
            var endpoint = 'topstories.json';
            var url = makeUrl(baseUrl, endpoint);
            return axios.get(url.href)
        },

        storyById: function(storyId) {
            var endpoint = 'item/' + storyId + '.json'
            var url = makeUrl(baseUrl, endpoint)
            return axios.get(url.href)
        },
    }
}

exports.baseUrl = baseUrl;
exports.createClient = createClient;
