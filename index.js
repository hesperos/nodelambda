hn = require('./hackernews')

client = hn.createClient(hn.baseUrl);

ids = client.topStories()
    .then(resp => {
        const maxItems = 10;
        return resp.data.slice(0, maxItems);
    })
    .catch(err => {
        console.log(err);
    });

var titles = ids.then(async (ids) => {
    var titlePromises = [];
    for (const storyId of ids) {
        title = client.storyById(storyId)
            .then(resp => {
                return resp.data.title;
            })
            .catch(err => {
                console.log(err);
            });

        titlePromises.push(title);
    }

    titles = await Promise.all(titlePromises);
    return titles;
});

titles.then(titles => {
    var welcomeLine = 'Here are some HN titles:\n\n';
    var titles = titles.join("\n");
    var message = welcomeLine + titles;

    console.log(message)
})
